(function () {
    class SlideShow {
        constructor(startIndex, element, autoplay = false) {
            this.startIndex = startIndex;
            this.currentIndex = this.startIndex;
            this.element = element;
            this.slides = this.element.querySelectorAll('.slide');
            this.dots = this.element.querySelectorAll('.slideshow__dot');
            this.setActiveSlide();
            this.next();
            this.prev();
            if (autoplay) this.setAutoplay();
            if (this.dots) this.setDotsActivate()
        }

        setActiveSlide() {
            this.slides.forEach((item, index) => {
                if (index === this.currentIndex) {
                    item.classList.add('active');
                } else {
                    item.classList.remove('active');
                }
            })
        }

        setDotsActivate() {
            this.dots.forEach((item, index) => {
                item.addEventListener('click', () => {
                    this.currentIndex = index;
                    this.setActiveSlide();
                })
            })
        }

        setAutoplay() {
            const time = this.element.getAttribute('data-autoplay');
            setInterval(() => {
                this.goToNextSlide();
            }, time)
        }

        next() {
            let nextBtn = this.element.querySelector('[data-way="next"]');
            nextBtn.addEventListener('click', () => {
                this.goToNextSlide();
            })
        }

        goToNextSlide() {
            if (this.currentIndex === this.slides.length - 1) {
                this.currentIndex = 0;
            } else {
                this.currentIndex++;
            }
            this.setActiveSlide();
        }

        prev() {
            let prevBtn = this.element.querySelector('[data-way="prev"]');
            prevBtn.addEventListener('click', () => {
                if (this.currentIndex === 0) {
                    this.currentIndex = this.slides.length - 1;
                } else {
                    this.currentIndex--;
                }
                this.setActiveSlide();
            })
        }
    }

    let slideShow = document.querySelectorAll('.slideshow');

    slideShow.forEach(item => {
        new SlideShow(0, item, item.hasAttribute('data-autoplay'))
    })


}());
